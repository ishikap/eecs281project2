#include <getopt.h>
#include <iostream>
#include <string>
#include <vector>
#include <algorithm>
#include <deque>
#include <limits>
#include <utility>
#include <iomanip>
#include "P2.h"
#include "Eecs281PQ.h"
#include "PoorManPQ.h"
#include "BinaryPQ.h"
#include "SortedPQ.h"
#include "PairingPQ.h"
using namespace std;


struct Tile
{
  pair<short, short> coor;
  short val;
  bool seen=false;
  bool TNTseen=false;
};

struct comp
{
  bool operator()(Tile* a, Tile* b)
  {
    if(a->val==b->val)
    {
      if(a->coor.second==b->coor.second)
      {
        return a->coor.first>b->coor.first;
      }
      else
      {
        return a->coor.second>b->coor.second;
      }
    }
    else
    {
      return a->val>b->val;
    }
  }
};

struct compoutput
{
  bool operator()(Tile a, Tile b)
  {
    if(a.val==b.val)
    {
      if(a.coor.second==b.coor.second)
      {
        return a.coor.first>b.coor.first;
      }
      else
      {
        return a.coor.second>b.coor.second;
      }
    }
    else
    {
      return a.val>b.val;
    }
  }
};



void TNTissue(vector<vector<Tile> > &map, short size, pair<short, short> current, Eecs281PQ<Tile*, comp> *TNTPQ, vector<Tile> &output, bool m, Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower);
void readMap(vector<vector<Tile> > &map, short size, istream &inputstream);
void routing(vector<vector<Tile> > &map, short size, pair<short, short> start, Eecs281PQ<Tile*, comp> *routingPQ, Eecs281PQ<Tile*, comp> *TNTPQ, vector<Tile> &output, bool m, Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower);
void pushToRoutingPQ(vector<vector<Tile> > &map, short size, pair<short,short> current, Eecs281PQ<Tile*, comp> *routingPQ);
void pushToTNTPQ(vector<vector<Tile> > &map, short size, pair<short,short> current, Eecs281PQ<Tile*, comp> *TNTPQ);
void printOutput(vector<Tile> output, int N, bool s);
double median(Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower, short val);



void readMap(vector<vector<Tile> > &map, short size, istream &inputstream)
{
  Tile temp;
  for(short row=0; row<size; ++row)
  {
    for(short col=0; col<size; ++col)
    {
      inputstream>>temp.val;
      temp.coor.first=row;
      temp.coor.second=col;
      
      map[row][col]=temp;
	  
    }
	
  }
}//readMap


void routing(vector<vector<Tile> > &map, short size, pair<short, short> start, Eecs281PQ<Tile*, comp> *routingPQ, Eecs281PQ<Tile*, comp> *TNTPQ, vector<Tile> &output, bool m, Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower)
{
 		pair<short, short> current=start;
  	while(current.first!=0 && current.first!=size-1 && current.second!=0 && current.second!=size-1)
		{
			if(m && map[current.first][current.second].val!=0 && map[current.first][current.second].val!=-1)
			{
			    std::cout<<std::fixed << std::setprecision( 2 );
					cout<<"Median difficulty of clearing rubble is: "<<median(medianUpper, medianLower, map[current.first][current.second].val)<<"\n";
			}//if!0,!-1
			//push to output if not 0
			if(map[current.first][current.second].val!=0 )
			{
				output.push_back(map[current.first][current.second]);
			}
			//if TNT
			if(map[current.first][current.second].val==-1)
			{
			  TNTissue(map,size,current,TNTPQ, output,m, medianUpper, medianLower);
			  if(routingPQ->size()>1)
				{
					routingPQ->updatePriorities();
				}
			}
			map[current.first][current.second].val=0;
			pushToRoutingPQ(map, size, current, routingPQ);
			//cout<<routingPQ->top()->val<<endl;
			current=routingPQ->top()->coor;
			routingPQ->pop();
	  }//while
	  if(m && map[current.first][current.second].val!=0 && map[current.first][current.second].val!=-1)
	  {
		 std::cout<<std::fixed << std::setprecision( 2 );
		 cout<<"Median difficulty of clearing rubble is: "<<median(medianUpper, medianLower, map[current.first][current.second].val)<<"\n";
	  }
	  if(map[current.first][current.second].val!=0)
	  {
			//cout<<"i was here"<<endl;
		  output.push_back(map[current.first][current.second]);
	  }
	 if(map[current.first][current.second].val==-1)
	 {
		TNTissue(map,size,current,TNTPQ, output,m, medianUpper, medianLower);
	 }
}


void pushToRoutingPQ(vector<vector<Tile> > &map, short size, pair<short,short> current, Eecs281PQ<Tile*, comp> *routingPQ)
{
	if(current.first!=0 && !map[current.first-1][current.second].seen)
 	 {
   	 	map[current.first-1][current.second].seen=true;
    	routingPQ->push(&map[current.first-1][current.second]);
 	 }
 	 if(current.second!=0 && !map[current.first][current.second-1].seen)
 	 {
   	 	map[current.first][current.second-1].seen=true;
    	routingPQ->push(&map[current.first][current.second-1]);
 	 }
 	 if(current.second!=size-1 && !map[current.first][current.second+1].seen)
 	 {
   		map[current.first][current.second+1].seen=true;
    	routingPQ->push(&map[current.first][current.second+1]);
 	 }
 	 if(current.first!=size-1 && !map[current.first+1][current.second].seen)
 	 {
    	map[current.first+1][current.second].seen=true;
    	routingPQ->push(&map[current.first+1][current.second]);
 	 }
}




void pushToTNTPQ(vector<vector<Tile> > &map, short size, pair<short,short> current, Eecs281PQ<Tile*, comp> *TNTPQ)
{
 // map[current.first][current.second].val=0;
  if(current.first!=0 && map[current.first-1][current.second].val!=0 && !map[current.first-1][current.second].TNTseen)
  {
	map[current.first-1][current.second].TNTseen=true;
    TNTPQ->push(&map[current.first-1][current.second]);
  }
  if(current.second!=0  && map[current.first][current.second-1].val!=0 && !map[current.first][current.second-1].TNTseen )
  {
	map[current.first][current.second-1].TNTseen=true;
    TNTPQ->push(&map[current.first][current.second-1]);
  }
  if(current.second<size-1  && map[current.first][current.second+1].val!=0 && !map[current.first][current.second+1].TNTseen)
  {
	map[current.first][current.second+1].TNTseen=true;
    TNTPQ->push(&map[current.first][current.second+1]);
  }
  if(current.first<size-1  && map[current.first+1][current.second].val!=0 && !map[current.first+1][current.second].TNTseen)
  {
	map[current.first+1][current.second].TNTseen=true;
    TNTPQ->push(&map[current.first+1][current.second]);
  }
}



void TNTissue(vector<vector<Tile> > &map, short size, pair<short, short> current, Eecs281PQ<Tile*, comp> *TNTPQ, vector<Tile> &output, bool m, Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower)
{
	  map[current.first][current.second].val=0;
    pushToTNTPQ(map, size, current, TNTPQ);
    while(!TNTPQ->empty())
    {
		if(TNTPQ->top()->val!=0)
		{
			output.push_back(*(TNTPQ->top()));
			if(m && TNTPQ->top()->val!=-1)
			{
		      std::cout<<std::fixed << std::setprecision( 2 );
			  cout<<"Median difficulty of clearing rubble is: "<<median(medianUpper, medianLower, TNTPQ->top()->val)<<"\n";
			}	
		}
		current=TNTPQ->top()->coor;
		TNTPQ->pop();
		if(map[current.first][current.second].val==-1)
		{
		  map[current.first][current.second].val=0;
	      pushToTNTPQ(map, size, current, TNTPQ);
		}
	     map[current.first][current.second].val=0;

 	 }//while
	
}//end of functiom

void printOutput(vector<Tile> output, int N, bool s)
{
  int NUM=0, AMOUNT=0;
  for(size_t i=0; i<output.size(); ++i)
	{
		if(output[i].val!=-1)
		{
		  ++NUM;
		  AMOUNT+=output[i].val;
		}
	}
  cout<<"Cleared "<<NUM<<" tiles containing "<<AMOUNT<<" rubble and escaped.\n";
	if(s)
	{
		cout<<"First tiles cleared:\n";
		for(int i=0; i<N && i<(int)output.size(); ++i)
		{
			if(output[i].val==-1)
			{
				cout<<"TNT at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
			else
			{
				cout<<output[i].val<<" at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";

			}
		}
		cout<<"Last tiles cleared:\n";
		for(int i=(int)output.size()-1; i>=0 && i>=(int)output.size()-N; --i)
		{
			if(output[i].val==-1)
			{
				cout<<"TNT at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
			else
			{
				cout<<output[i].val<<" at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
		}
		compoutput CompOutput;
		std::sort(output.begin(), output.end(), CompOutput);
		cout<<"Easiest tiles cleared:\n";
		for(int i=(int)output.size()-1; i>=0 && i>=(int)output.size()-N; --i)
		{
			if(output[i].val==-1)
			{
				cout<<"TNT at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
			else
			{
				cout<<output[i].val<<" at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
		}
		cout<<"Hardest tiles cleared:\n";
		for(int i=0; i<N && i<(int)output.size(); ++i)
		{
			if(output[i].val==-1)
			{
				cout<<"TNT at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
			else
			{
				cout<<output[i].val<<" at ["<<output[i].coor.first<<","<<output[i].coor.second<<"]\n";
			}
		}
	}
}

double median(Eecs281PQ<short, std::greater<short> > *medianUpper, Eecs281PQ<short, std::less<short> > *medianLower, short val)
{
    if(val>=medianUpper->top())
    {
      medianUpper->push(val);
    }
    else
    {
      medianLower->push(val);
    }
    //Rebalance
    if(medianUpper->size()-medianLower->size()==2)
    {
      medianLower->push(medianUpper->top());
      medianUpper->pop();
    }
    else if(medianLower->size()-medianUpper->size()==2)
    {
      medianUpper->push(medianLower->top());
      medianLower->pop();
    }
    //return median
    if(medianUpper->size()==medianLower->size())
    {
      return(medianUpper->top()+medianLower->top())/2.0;
    }
    else if(medianUpper->size()>medianLower->size())
    {
      return medianUpper->top();
    }
    else
    {
      return medianLower->top();
    }
}



int main(int argc, char *argv[])
{
  ios_base::sync_with_stdio(false);


  //parse in command line args
  bool c=false, s=false, m=false;
  string container;
  int N=0;
  int choice;
  int option_index = 0;

  option long_options[] =
  {
      { "container",  required_argument,  nullptr,    'c'},
      { "stats",      required_argument,  nullptr,    's'},
      { "median",     no_argument,        nullptr,    'm'},
      { "help",       no_argument,        nullptr,    'h'},
      { nullptr,  0,                  nullptr,   '\0' }
  };

  while ((choice = getopt_long(argc, argv, "c:s:mh", long_options, &option_index)) != -1) {
      switch(choice)
      {
          case 'c':
              c=true;
              container=optarg;
              if(container!="BINARY" && container!="POOR_MAN" && container!="SORTED" && container!="PAIRING")
              {
                cerr<<"Invalid Container"<<endl;
                exit(1);
              }
              break;

          case 's':
              s=true;
              N=atoi(optarg);
              break;

          case 'm':
              m=true;
              break;

          case 'h':
              cout << "Read the project Spec HAHAHA"<< endl;
              exit(0);
              break;

          default:
              cerr << "Error: invalid option " << endl;
              exit(1);
              break;
      } // switch
  } // while
  
  //check if required flag is present
  if(!c)
  {
    cerr<<"Container Required"<<endl;
    exit(1);
  }
  

  //map initial stuff
  char mapType;
  short mapSize;
  pair<short, short> start;
  string garbage;
  cin>>mapType>>garbage>>mapSize>>garbage>>start.first>>start.second;
  
  //map error checking
  if(mapType!='M' && mapType!='R')
  {
    cerr<<"Undefined Map Type"<<endl;
    exit(1);
  }
  if(mapSize<1)
  {
    cerr<<"Map Size needs to be positive, non-zero"<<endl;
    exit(1);
  }
  if(start.first<0 || start.second<0 || start.first>=mapSize || start.second>=mapSize)
  {
    cerr<<"start out of bounds"<<endl;
    exit(1);
  }
  //map pseudoRandom stuff
  stringstream ss;
  if(mapType=='R')
  {
	int seed, maxRubble, TNT;
    cin>>garbage>>seed>>garbage>>maxRubble>>garbage>>TNT;
    P2::PR_init(ss, seed, mapSize, maxRubble, TNT);
  }
  istream &inputstream = (mapType=='M') ? cin : ss;
  //createmap,resize,read in
  vector<vector<Tile> > map;
  map.resize(mapSize, vector<Tile>(mapSize));
  readMap(map, mapSize, inputstream);
  
	//determine PQ
  comp Comparator;
  Eecs281PQ<Tile*, comp> *routingPQ;
  Eecs281PQ<Tile*, comp> *TNTPQ;
  Eecs281PQ<short, std::greater<short> > *medianUpper;
  Eecs281PQ<short, std::less<short> > *medianLower;
  if(container=="POOR_MAN")
  {
    routingPQ = new PoorManPQ<Tile*, comp>(Comparator);
    TNTPQ = new PoorManPQ<Tile*, comp>(Comparator);
    medianUpper = new PoorManPQ<short, std::greater<short> >(std::greater<short>());
    medianLower = new PoorManPQ<short, std::less<short> >(std::less<short>());
	  cout<<"Using POORMAN priority queue.\n";
  }
  else if(container=="BINARY")
  {
    routingPQ = new BinaryPQ<Tile*, comp>(Comparator);
    TNTPQ = new BinaryPQ<Tile*, comp>(Comparator);
    medianUpper = new BinaryPQ<short, std::greater<short> >(std::greater<short>());
    medianLower = new BinaryPQ<short, std::less<short> >(std::less<short>());
	  cout<<"Using BINARY priority queue.\n";
  }
  else if(container=="SORTED")
  {
    routingPQ = new SortedPQ<Tile*, comp>(Comparator);
    TNTPQ = new SortedPQ<Tile*, comp>(Comparator);
    medianUpper = new SortedPQ<short, std::greater<short> >(std::greater<short>());
    medianLower = new SortedPQ<short, std::less<short> >(std::less<short>());
	  cout<<"Using SORTED priority queue.\n";
  }
  else
  {
    routingPQ = new PairingPQ<Tile*, comp>(Comparator);
    TNTPQ = new PairingPQ<Tile*, comp>(Comparator);
    medianUpper = new PairingPQ<short, std::greater<short> >(std::greater<short>());
    medianLower = new PairingPQ<short, std::less<short> >(std::less<short>());
	  cout<<"Using PAIRING priority queue.\n";
  }
  
  medianUpper->push(std::numeric_limits<short>::max());
  medianLower->push (std::numeric_limits<short>::min());
  
  //routing
	vector<Tile> output;
  map[start.first][start.second].seen=true;
  routingPQ->push(&map[start.first][start.second]);
	routing(map,mapSize, start, routingPQ, TNTPQ, output, m, medianUpper, medianLower);

  printOutput(output, N, s);
  
  delete routingPQ;
  delete TNTPQ;
  delete medianUpper;
  delete medianLower;
}//main
