#ifndef PAIRING_PQ_H
#define PAIRING_PQ_H

#include "Eecs281PQ.h"
#include <vector>
#include <deque>
//A specialized version of the priority queue ADT implemented as a pairing heap.

template<typename TYPE, typename COMP_FUNCTOR = std::less<TYPE>>
class PairingPQ : public Eecs281PQ<TYPE, COMP_FUNCTOR> {
public:
    //Description: Construct a priority queue out of an iterator range with an optional
    //             comparison functor.
    //Runtime: O(n) where n is number of elements in range.
    template<typename InputIterator>
        PairingPQ(InputIterator start, InputIterator end, COMP_FUNCTOR comp = COMP_FUNCTOR());

    //Description: Construct an empty priority queue with an optional comparison functor.
    //Runtime: O(1)
    PairingPQ(COMP_FUNCTOR comp = COMP_FUNCTOR());

    //Description: Copy constructor.
    //Runtime: O(n)
    PairingPQ(const PairingPQ &other);

    //Description: Copy assignment operator.
    //Runtime: O(n)
    PairingPQ &operator=(const PairingPQ &rhs);

    //Description: Destructor
    //Runtime: O(n)
    ~PairingPQ();

    //Description: Add a new element to the priority_queue. This has been provided for you,
    //             in that you should implement push functionality in the
    //             addNode function.
    //Runtime: Amortized O(1)
    virtual void push(const TYPE& val) {addNode(val); }

    //Description: Remove the most extreme (defined by 'compare') element from
    //             the priority queue.
    //Note: We will not run tests on your code that would require it to pop an
    //element when the priority queue is empty. Though you are welcome to if you are
    //familiar with them, you do not need to use exceptions in this project.
    //Runtime: Amortized O(log n)
    virtual void pop();

    //Description: Return the most extreme (defined by 'compare') element of
    //             the priority queue.
    //Runtime: O(1)
    virtual const TYPE &top() const;

    //Description: Get the number of elements in the priority queue.
    //Runtime: O(1)
    virtual std::size_t size() const {
        //TOD: Implement this function
        return PQsize; // TOD: Delete or change this line
    }

    //Description: Return true if the priority queue is empty.
    //Runtime: O(1)
    virtual bool empty() const {
        //TOD: Implement this function
        return root==nullptr; // TOD: Delete or change this line
    }

    //Description: Assumes that all elements in the priority queue are out of order.
    //             You must reorder the data so that the PQ invariant is restored.
    //Runtime: O(n)
    virtual void updatePriorities();

    // Each node within the pairing heap
    class Node {
        public:
            Node(const TYPE &val)
                : elt(val), sibling(nullptr), child(nullptr), previous(nullptr)
            {}
        public:
            //Description: Allows access to the element at that Node's position.
            //Runtime: O(1) - this has been provided for you.
            const TYPE &operator*() const { return elt; }
            const Node *sibling_ptr() const { return sibling; }
            const Node *child_ptr() const { return child; }

            //The following line allows you to access any private data members of this
            //Node class from within the PairingPQ class. (ie: myNode.elt is a legal
            //statement in PairingPQ's add_node() function).
            friend PairingPQ;
        private:
            TYPE elt;
            Node *sibling;
            Node *child;
			Node *previous;
            //TOD: Add one extra pointer (parent or previous) as desired.
    };

    //Description: Updates the priority of an element already in the priority_queue by
    //             replacing the element refered to by the Node with new_value.
    //             Must maintain priority_queue invariants.
    //
    //PRECONDITION: The new priority, given by 'new_value' must be more extreme
    //              (as defined by comp) than the old priority.
    //
    //Runtime: As discussed in reading material.
    void updateElt(Node* node, const TYPE &new_value);

    //Description: Add a new element to the priority_queue. Returns a Node* corresponding
    //             to the newly added element.
    //Runtime: Amortized O(1)
    Node* addNode(const TYPE& val);

    const Node *root_ptr() const { return root; }

private:
	size_t PQsize;
    Node *root;
	void destructorHelper();
	void cleanUp(Node *x);
	Node* copy(Node *x);
	Node* meld(Node *x);
	void join(Node* &x, Node* y);

    //TOD: Add any additional member functions or data you require here.
    //TOD: We recommend creating a 'meld' function (see the Pairing Heap papers).
};


//Description: Construct a priority queue out of an iterator range with an optional
//             comparison functor.
//Runtime: O(n) where n is number of elements in range.
//TOD: when you implement this function, uncomment the parameter names
template<typename TYPE, typename COMP_FUNCTOR>
template<typename InputIterator>
PairingPQ<TYPE, COMP_FUNCTOR>::PairingPQ(
        InputIterator  start ,
        InputIterator  end ,
        COMP_FUNCTOR  comp 
        ) 
{
    //TOD: Implement this function
    this->compare=comp;
	PQsize=0;
	while(start!=end)
	{
		addNode(*start);
		++start;	
	}
}


//Description: Construct an empty priority queue with an optional comparison functor.
//Runtime: O(1)
//TOD: when you implement this function, uncomment the parameter name
template<typename TYPE, typename COMP_FUNCTOR>
PairingPQ<TYPE, COMP_FUNCTOR>::PairingPQ(COMP_FUNCTOR  comp ) 
{
    //TOD: Implement this function
   	 root=nullptr;
	 PQsize=0;
	 this->compare=comp;
}


//Description: Copy constructor.
//Runtime: O(n)
//TOD: when you implement this function, uncomment the parameter name
template<typename TYPE, typename COMP_FUNCTOR>
PairingPQ<TYPE, COMP_FUNCTOR>::PairingPQ(const PairingPQ &  other ) 
{
    //TOD: Implement this function
    root=nullptr;
	*this=other;
	PQsize=other.size();
}


//Description: Copy assignment operator.
//Runtime: O(n)
//TOD: when you implement this function, uncomment the parameter name
template<typename TYPE, typename COMP_FUNCTOR>
PairingPQ<TYPE, COMP_FUNCTOR>&
PairingPQ<TYPE, COMP_FUNCTOR>::operator=(const PairingPQ &  rhs ) 
{
    //TOD: Implement this function
    //deep copy
    if(this!= &rhs)
	{
		destructorHelper();
		root=copy(rhs.root);
	}
	PQsize=rhs.size();
    return *this;
}


//Description: Destructor
//Runtime: O(n)
template<typename TYPE, typename COMP_FUNCTOR>
PairingPQ<TYPE, COMP_FUNCTOR>::~PairingPQ() 
{
    //TOD: Implement this function
    destructorHelper();
}



template<typename TYPE, typename COMP_FUNCTOR>
void PairingPQ<TYPE, COMP_FUNCTOR>::destructorHelper() 
{

    cleanUp(root);
	root=nullptr;
	PQsize=0;
}


//Description: Remove the most extreme (defined by 'compare') element from
//             the priority queue.
//Note: We will not run tests on your code that would require it to pop an
//element when the priority queue is empty. Though you are welcome to if you are
//familiar with them, you do not need to use exceptions in this project.
//Runtime: Amortized O(log n)
template<typename TYPE, typename COMP_FUNCTOR>
void PairingPQ<TYPE, COMP_FUNCTOR>::pop() 
{
    //TOD: Implement this function
	Node *temp=root;
	if(root->child==nullptr)
	{
		root=nullptr;
	}
	else
	{	
		root=meld(root->child);
	}
	delete temp;
	--PQsize;
}


//Description: Return the most extreme (defined by 'compare') element of
//             the priority queue.
//Runtime: O(1)
template<typename TYPE, typename COMP_FUNCTOR>
const TYPE &PairingPQ<TYPE, COMP_FUNCTOR>::top() const 
{
    //TOD: Implement this function

    //These lines are present only so that this provided file compiles.
   // static TYPE temp; //TOD: Delete this line
    return root->elt;      //TOD: Delete or change this line
}


//Description: Assumes that all elements in the priority queue are out of order.
//             You must reorder the data so that the PQ invariant is restored.
//Runtime: O(n log n)
template<typename TYPE, typename COMP_FUNCTOR>
void PairingPQ<TYPE, COMP_FUNCTOR>::updatePriorities() 
{
    //TOD: Implement this function
	PairingPQ<TYPE, COMP_FUNCTOR> *newPQ=new PairingPQ<TYPE, COMP_FUNCTOR>(this->compare);
    while(!empty())
	{
			newPQ->addNode(root->elt);
			pop();
	}
	*this=*newPQ;
   /*
	Node* x;
	PairingPQ<TYPE, COMP_FUNCTOR> *newPQ=new PairingPQ<TYPE, COMP_FUNCTOR>(this->compare);
	std::deque<Node*> temp;
	if(root!=nullptr)
	{
		newPQ->addNode(root->elt);
		if(root->child!=nullptr)
		{
			temp.push_front(root->child);
		}
	}
	
	while(!temp.empty())
	{
		x=temp.back();
		newPQ->addNode(x->elt);
		temp.pop_back();
		if(x->child!=nullptr)
		{
			temp.push_front(x->child);
		}
		if(x->sibling!=nullptr)
		{
			temp.push_front(x->sibling);
		}
	
	}
	*this=*newPQ;
	*/
	/*if(root!=nullptr)
	{
		meld(root);
	}*/	
}





//Description: Updates the priority of an element already in the priority_queue by
//             replacing the element refered to by the Node with new_value.
//             Must maintain priority_queue invariants.
//
//PRECONDITION: The new priority, given by 'new_value' must be more extreme
//              (as defined by comp) than the old priority.
//
//Runtime: As discussed in reading material.
//TOD: when you implement this function, uncomment the parameter names
template<typename TYPE, typename COMP_FUNCTOR>
void PairingPQ<TYPE, COMP_FUNCTOR>::updateElt(Node *  node, const TYPE & new_value ) 
{
    //TOD: Implement this function
	node->elt=new_value;
	if(node!=root)
	{
		if(node->previous->child==node)
		{
			node->previous->child=node->sibling;
			if(node->sibling!=nullptr)
			{
				node->sibling->previous=node->previous;
			}
		}
		else
		{
			node->previous->sibling=node->sibling;
			if(node->sibling!=nullptr)
			{
				node->sibling->previous=node->previous;
			}
		}
		node->previous=nullptr;
		node->sibling=nullptr;
		join(root, node);
	}
}


//Description: Add a new element to the priority_queue. Returns a Node* corresponding
//             to the newly added element.
//Runtime: Amortized O(1)
//TOD: when you implement this function, uncomment the parameter name
template<typename TYPE, typename COMP_FUNCTOR>
typename PairingPQ<TYPE, COMP_FUNCTOR>::Node*
PairingPQ<TYPE, COMP_FUNCTOR>::addNode(const TYPE &  val ) 
{
    //TOD: Implement this function
	Node *newNode=new Node(val);
	if(root==nullptr)
	{
		root=newNode;
	}
	else
	{
		join(root, newNode);
	}
	++PQsize;
    //These lines are present only so that this provided file compiles.
    return newNode; //TOD: Delete or change this line
}


template<typename TYPE, typename COMP_FUNCTOR>
void PairingPQ<TYPE, COMP_FUNCTOR>::cleanUp(Node *x) 
{
	if(x!=NULL &&  x->child!=x && x->sibling!=x)
	{
		cleanUp(x->child);
		cleanUp(x->sibling);
		delete x;
	}
	PQsize=0;
}



template<typename TYPE, typename COMP_FUNCTOR>
typename PairingPQ<TYPE, COMP_FUNCTOR>::Node*
 PairingPQ<TYPE, COMP_FUNCTOR>::copy(Node *x) 
{
    if(x==nullptr || x->child==x)
	{
		return nullptr;
	}
	Node *newNode = new Node(x->elt);
	newNode->child=copy(x->child);
	if(newNode->child!=nullptr)
	{
		newNode->child->previous=newNode;
	}
	newNode->sibling=copy(x->sibling);
	if(newNode->sibling!=nullptr)
	{
		newNode->sibling->previous=newNode;
	}
	return newNode;	
}




template<typename TYPE, typename COMP_FUNCTOR>
typename PairingPQ<TYPE, COMP_FUNCTOR>::Node*
 PairingPQ<TYPE, COMP_FUNCTOR>::meld(Node *x)
 {
	if(x->sibling==nullptr)
	{
		return x;
	}
	std::vector<Node*> data;
	int siblingSize=0;
	while(x!=nullptr)
	{
		data.push_back(x);
		x->previous->sibling=nullptr;
		x=x->sibling;
		siblingSize++;		
	}
	
	int i=0;
	for(i=0; i<(int)data.size()-1; i+=2)
	{
		join(data[i], data[i+1]);
	}
	int j=i-2;
	if(j==siblingSize-3)
	{
		join(data[j], data[j+2]);
	}
	for(; j>=2; j-=2)
	{
		join(data[j-2], data[j]);
	}
	return data[0];
}



template<typename TYPE, typename COMP_FUNCTOR>
void  PairingPQ<TYPE, COMP_FUNCTOR>::join(Node* &x, Node* y)
{
	if(y==nullptr || x==nullptr)
	{
		return;
	}
	if(this->compare(x->elt, y->elt))
	{
		y->previous=x->previous;
		x->previous=y;
		x->sibling=y->child;
		if(x->sibling!=nullptr)
		{
			x->sibling->previous=x;	
		}
		y->child=x;
		x=y;
	}
	else
	{
	    y->previous=x;
        x->sibling=y->sibling;
        if(x->sibling!=nullptr)
		{
            x->sibling->previous=x;
		}
        y->sibling=x->child;
        if(y->sibling!=nullptr)
		{
            y->sibling->previous=y;
		}
        x->child=y;
	}
	
}
#endif //PAIRING_H
